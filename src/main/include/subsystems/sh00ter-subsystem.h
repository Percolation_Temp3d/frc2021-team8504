// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <frc2/command/SubsystemBase.h>
#include <rev/CANSparkMax.h>

#include "Constants.h"

class OuttakeSubsystem : public frc2::SubsystemBase {
 public:
  OuttakeSubsystem();
  void Shoot();
  void Rotate();

 private:
  // The motor controllers
  rev::CANSparkMax outtake_motor1;
  rev::CANSparkMax outtake_motor2;
};