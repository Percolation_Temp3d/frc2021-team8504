// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <frc/Joystick.h>
#include <frc/TimedRobot.h>
#include <frc/Ultrasonic.h>
#include <photonlib/PhotonCamera.h>

#include "Constants.h"
#include "robot_container.h"
#include "subsystems/drive-subsystem.h"
#include "subsystems/intake-subsystem.h"
#include "subsystems/sh00ter-subsystem.h"

class Robot : public frc::TimedRobot {
 public:
  void RobotInit() override;
  void RobotPeriodic() override;
  void DisabledInit() override;
  void DisabledPeriodic() override;
  void AutonomousInit() override;
  void AutonomousPeriodic() override;
  void TeleopInit() override;
  void TeleopPeriodic() override;
  void TestPeriodic() override;

 private:
  // Have it null by default so that if testing teleop it
  // doesn't have undefined behavior and potentially crash
  frc2::Command* m_autonomouscommand = nullptr;
  RobotContainer m_container;

  // Logitech F310 default from Andymark
  frc::Joystick* gamepad = new frc::Joystick(JoystickSetup::kJoystickIndex);

  // Change this to match the name of your camera
  photonlib::PhotonCamera camera{"photonvision"};

  // PID turn controllers, aren't we using ours?
  frc2::PIDController forward_controller{
      PIDConstants::kLinearP, PIDConstants::kBothAxesI, PIDConstants::kLinearD};
  frc2::PIDController turn_controller{PIDConstants::kAngularP,
                                      PIDConstants::kBothAxesI,
                                      PIDConstants::kAngularD};

  // Diff drive
  DriveSubsystem driving_system;
  OuttakeSubsystem outtake_system;
  IntakeSubsystem intake_system;
};