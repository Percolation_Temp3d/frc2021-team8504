// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <frc/trajectory/constraint/DifferentialDriveKinematicsConstraint.h>
#include <units/velocity.h>
#include <units/voltage.h>

#include <wpi/math>

/**
 * The Constants header provides a convenient place for teams to hold robot-wide
 * numerical or bool constants.  This should not be used for any other purpose.
 *
 * It is generally a good idea to place constants into subsystem- or
 * command-specific namespaces within this header, which can then be used where
 * they are needed.
 */
namespace DriveConstants {
constexpr int kLeftMotor1Port = 3;
constexpr int kLeftMotor2Port = 4;
constexpr int kRightMotor1Port = 7;
constexpr int kRightMotor2Port = 8;
constexpr int kIntakeMotorMainPort = 1;
constexpr int kConveyerMotorMainPort = 9;
constexpr int kShooterNeo1 = 5;
constexpr int kShooterNeo2 = 6;

constexpr double safe_intake_power = 0.5;
constexpr double safe_conveyer_power = 0.3;

constexpr double max_intake_power = 1.0;
constexpr double max_conveyer_power = 1.0;

constexpr int kLeftEncoderPorts[]{0, 1};
constexpr int kRightEncoderPorts[]{2, 3};
constexpr bool kLeftEncoderReversed = false;
constexpr bool kRightEncoderReversed = true;

constexpr auto kTrackwidth = 0.01_m;
extern const frc::DifferentialDriveKinematics kDriveKinematics;

constexpr int kEncoderCPR = 1024;
constexpr double kWheelDiameterInches = 6;
constexpr double kEncoderDistancePerPulse =
    // Assumes the encoders are directly mounted on the wheel shafts
    (kWheelDiameterInches * wpi::math::pi) / static_cast<double>(kEncoderCPR);

// These are example values only - DO NOT USE THESE FOR YOUR OWN ROBOT!
// These characterization values MUST be determined either experimentally or
// theoretically for *your* robot's drive. The Robot Characterization
// Toolsuite provides a convenient tool for obtaining these values for your
// robot
constexpr auto ks = 0.536_V;
constexpr auto kv = 11.9 * 1_V * 1_s / 1_m;
constexpr auto ka = 0.0682 * 1_V * 1_s * 1_s / 1_m;

// Example value only - as above, this must be tuned for your drive!
constexpr double kPDriveVel = 0.0;
}  // namespace DriveConstants

namespace AutoConstants {
// Speed
constexpr auto kMaxSpeed = 3_mps;
constexpr auto kMaxAcceleration = 3_mps_sq;

// Reasonable baseline values for a RAMSETE follower in units of meters and
// seconds
constexpr double kRamseteB = 2;
constexpr double kRamseteZeta = 0.7;
}  // namespace AutoConstants

namespace OIConstants {
constexpr int kDriverControllerPort = 1;
}  // namespace OIConstants

namespace PIDConstants {
// PID constants should be tuned per robot
constexpr double kLinearP = 0.0;
constexpr double kLinearD = 0.0;
constexpr double kBothAxesI = 0.0;
constexpr double kAngularP = 0.0;
constexpr double kAngularD = 0.0;
}  // namespace PIDConstants

namespace FieldMetrics {
// Constant +- distance from robot to closest (always red) ball
constexpr int kClosestDistanceA = 160;
constexpr int kClosestDistanceB = 150;
}  // namespace FieldMetrics

namespace JoystickSetup {
// Driver station constant
constexpr int kJoystickIndex = 0;

// Sensitivity
constexpr double KCoefficient = 0.1;

// Gamepad button numbers
constexpr int kGamepadButtonA = 1;
constexpr int kGamepadButtonB = 2;
constexpr int kGamepadButtonX = 3;
constexpr int kGamepadButtonY = 4;
constexpr int kGamepadButtonShoulderL = 5;
constexpr int kGamepadButtonShoulderR = 6;
constexpr int kGamepadButtonBack = 7;
constexpr int kGamepadButtonStart = 8;
constexpr int kGamepadButtonLeftStick = 9;
constexpr int kGamepadButtonRightStick = 10;
}  // namespace JoystickSetup

namespace PhotonVisionSettings {
constexpr units::meter_t kCameraHeight = 1.6667_ft;
constexpr units::meter_t kTargetHeight = 5_ft;
// Angle between horizontal and the camera
constexpr units::radian_t kCameraPitch = 0_deg;

// How far from the target we want to be
constexpr units::meter_t kZoneGoalRanges[5] = {30_ft, 25_ft, 19_ft, 14_ft,
                                               9_ft};
}  // namespace PhotonVisionSettings