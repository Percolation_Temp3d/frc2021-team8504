// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <frc/Ultrasonic.h>
#include <frc/XboxController.h>
#include <frc/controller/PIDController.h>
#include <frc/smartdashboard/SendableChooser.h>
#include <frc/trajectory/Trajectory.h>
#include <frc2/command/Command.h>
#include <frc2/command/InstantCommand.h>
#include <frc2/command/PIDCommand.h>
#include <frc2/command/ParallelRaceGroup.h>
#include <frc2/command/RunCommand.h>
#include <wpi/Path.h>
#include <wpi/SmallString.h>

#include <string>

#include "Constants.h"
#include "subsystems/drive-subsystem.h"

using std::string;

/**
 * This class is where the bulk of the robot should be declared.  Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls).  Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
class RobotContainer {
 public:
  RobotContainer();

  frc2::Command* GetAutonomousCommand();

  struct PathFileNames {
    // Galactic search A task
    const string kPathAblue = "FinalPathA_blue.wpilib.json";
    const string kPathAred = "FinalPathA_red.wpilib.json";

    // Galactic search B task
    const string kPathBblue = "FinalPathB_blue.wpilib.json";
    const string kPathBred = "FinalPathB_red.wpilib.json";

    // Other pathes
    const string kPathBarrel = "BR_path.wpilib.json";
    const string kPathBounce = "B_path.wpilib.json";
    const string kPathSlalom = "S_path.wpilib.json";

    // For now
    string selected_way;

    // Getters / setters
    void SetWay(string s) { selected_way = s; }
    string GetWay() { return selected_way; }
  };

  enum StartsWithLetterMode {
    A_GALACTIC_SEARCH,
    B_GALACTIC_SEARCH,
    BARREL_TASK,
    BOUNCE_TASK,
    SLALOM_PATH
  };

 private:
  // Directory for deploying
  wpi::SmallString<64> deploy_directory;
  // Trajectory
  frc::Trajectory main_trajectory;

  // The driver's controller
  frc::XboxController m_drivercontroller{OIConstants::kDriverControllerPort};

  // The robot's subsystems and commands are defined here...

  // The robot's subsystems
  DriveSubsystem m_drive;

  // For joystick
  frc2::InstantCommand m_drive_halfspeed{[this] { m_drive.SetMaxOutput(0.5); },
                                         {}};
  frc2::InstantCommand m_drive_fullspeed{[this] { m_drive.SetMaxOutput(1); },
                                         {}};

  // The chooser for the autonomous routines
  frc::SendableChooser<frc2::Command*> m_chooser;

  // Creates a ping-response Ultrasonic object on DIO 1 and 2.
  frc::Ultrasonic ultrasonic{1, 2};

  // Here is de way:
  PathFileNames the_endway;

  void ConfigureButtonBindings();
  void ConfigureDeployFolder();
  void SelectTrajectoryPath();
};