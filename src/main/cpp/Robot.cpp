// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "robot.h"

#include <frc/smartdashboard/SmartDashboard.h>
#include <frc2/command/CommandScheduler.h>
#include <photonlib/PhotonUtils.h>

#include "Constants.h"

void Robot::RobotInit() {}

/**
 * This function is called every robot packet, no matter the mode. Use this for
 * items like diagnostics that you want to run during disabled, autonomous,
 * teleoperated and test.
 *
 * This runs after the mode specific periodic functions, but before LiveWindow
 * and SmartDashboard integrated updating.
 */
void Robot::RobotPeriodic() { frc2::CommandScheduler::GetInstance().Run(); }

/**
 * This function is called once each time the robot enters Disabled mode. You
 * can use it to reset any subsystem information you want to clear when the
 * robot is disabled.
 */
void Robot::DisabledInit() {}

void Robot::DisabledPeriodic() {}

/**
 * This autonomous runs the autonomous command selected by your {@link
 * RobotContainer} class.
 */
void Robot::AutonomousInit() {
  m_autonomouscommand = m_container.GetAutonomousCommand();

  if (m_autonomouscommand != nullptr) {
    m_autonomouscommand->Schedule();
  }
}

void Robot::AutonomousPeriodic() { intake_system.RunSafe(); }

void Robot::TeleopInit() {
  // This makes sure that the autonomous stops running when teleop starts
  // running. If you want the autonomous to continue until interrupted by
  // another command, remove this line or comment it out

  if (m_autonomouscommand != nullptr) {
    m_autonomouscommand->Cancel();
    m_autonomouscommand = nullptr;
  }
}

/**
 * This function is called periodically during operator control.
 */
void Robot::TeleopPeriodic() {
  // If we have no targets, stay still
  // Manual Driver Mode
  double forward_speed =
             -1.0 * gamepad->GetY(frc::GenericHID::JoystickHand::kRightHand),
         rotation_speed =
             gamepad->GetX(frc::GenericHID::JoystickHand::kLeftHand),
         degrees = 0.0;

  if (gamepad->GetRawButton(JoystickSetup::kGamepadButtonA)) {
    // Vision-alignment mode
    // Query the latest result from PhotonVision
    const auto& result = camera.GetLatestResult();
    bool non_manual = result.HasTargets();

    if (non_manual) {
      /*
      // First calculate range
      units::meter_t range = photonlib::PhotonUtils::CalculateDistanceToTarget(
          PhotonVisionSettings::kCameraHeight,
          PhotonVisionSettings::kTargetHeight,
          PhotonVisionSettings::kCameraPitch,
          units::degree_t{result.GetBestTarget().GetPitch()});

      // Use this range as the measurement we give to the PID controller.
      // -1.0 required to ensure positive PID controller effort _increases_
      // range
      forward_speed =
          -1.0 * forward_controller.Calculate(
                     range.to<double>(),
                     PhotonVisionSettings::kZoneGoalRanges[current_goal_index]
                         .to<double>());

      // Also calculate angular power
      // -1.0 required to ensure positive PID controller effort _increases_
      // yaw
      rotation_speed =
          -1.0 * turn_controller.Calculate(result.GetBestTarget().GetYaw(), 0);
      */

      // Get degrees for gun rotation as well - pitch
      degrees = result.GetBestTarget().GetPitch();
    }
  } else if (gamepad->GetRawButtonPressed(JoystickSetup::kGamepadButtonX)) {
  } else if (gamepad->GetRawButtonPressed(JoystickSetup::kGamepadButtonY)) {
  }

  // Use our forward/turn speeds to control the drivetrain
  driving_system.ArcadeDrive(forward_speed, rotation_speed);
  // Set distance for the next zone
  // current_goal++;
}

/**
 * This function is called periodically during test mode.
 */
void Robot::TestPeriodic() {}

#ifndef RUNNING_FRC_TESTS
int main() { return frc::StartRobot<Robot>(); }
#endif