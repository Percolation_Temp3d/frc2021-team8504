// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "robot_container.h"

#include <frc/Filesystem.h>
#include <frc/controller/PIDController.h>
#include <frc/controller/RamseteController.h>
#include <frc/smartdashboard/SmartDashboard.h>
#include <frc/trajectory/Trajectory.h>
#include <frc/trajectory/TrajectoryGenerator.h>
#include <frc/trajectory/TrajectoryUtil.h>
#include <frc/trajectory/constraint/DifferentialDriveVoltageConstraint.h>
#include <frc2/command/InstantCommand.h>
#include <frc2/command/RamseteCommand.h>
#include <frc2/command/SequentialCommandGroup.h>
#include <frc2/command/button/JoystickButton.h>
#include <wpi/Path.h>
#include <wpi/SmallString.h>

#include "Constants.h"
#include "subsystems/drive-subsystem.h"

using std::string;

RobotContainer::RobotContainer() {
  // Initialize all of your commands and subsystems here

  // Starts the ultrasonic sensor running in automatic mode
  ultrasonic.SetAutomaticMode(true);
  // Places the ultrasonic on the dashboard
  frc::SmartDashboard::PutData("Ultrasonic", &ultrasonic);
  // Configure the button bindings
  ConfigureButtonBindings();

  // Set up default drive command
  m_drive.SetDefaultCommand(frc2::RunCommand(
      [this] {
        m_drive.ArcadeDrive(
            m_drivercontroller.GetY(frc::GenericHID::kLeftHand),
            m_drivercontroller.GetX(frc::GenericHID::kRightHand));
      },
      {&m_drive}));

  ConfigureDeployFolder();

  // Initialize trajectory
  try {
    main_trajectory = frc::TrajectoryUtil::FromPathweaverJson(deploy_directory);
  } catch (const std::exception& e) {
    // Probably incorrect path, check it out
    wpi::outs() << e.what() << "\n";
  }
}

void RobotContainer::ConfigureDeployFolder() {
  // Makes a deploy folder with paths in it
  frc::filesystem::GetDeployDirectory(deploy_directory);
  wpi::sys::path::append(deploy_directory, "paths");

  wpi::sys::path::append(deploy_directory, the_endway.GetWay());
}

void RobotContainer::ConfigureButtonBindings() {
  // Configure your button bindings here

  // While holding the shoulder button, drive at half speed
  frc2::JoystickButton(&m_drivercontroller, JoystickSetup::kGamepadButtonB)
      .WhenPressed(&m_drive_halfspeed)
      .WhenReleased(&m_drive_fullspeed);
}

frc2::Command* RobotContainer::GetAutonomousCommand() {
  // Create a voltage constraint to ensure we don't accelerate too fast
  frc::DifferentialDriveVoltageConstraint autoVoltageConstraint(
      frc::SimpleMotorFeedforward<units::meters>(
          DriveConstants::ks, DriveConstants::kv, DriveConstants::ka),
      DriveConstants::kDriveKinematics, 10_V);

  // Set up config for trajectory
  frc::TrajectoryConfig config(AutoConstants::kMaxSpeed,
                               AutoConstants::kMaxAcceleration);
  // Add kinematics to ensure max speed is actually obeyed
  config.SetKinematics(DriveConstants::kDriveKinematics);
  // Apply the voltage constraint
  config.AddConstraint(autoVoltageConstraint);

  /*
  // An example trajectory to follow.  All units in meters.
  auto exampleTrajectory = frc::TrajectoryGenerator::GenerateTrajectory(
    // Start at the origin facing the +X direction
    frc::Pose2d(0_m, 0_m, frc::Rotation2d(0_deg)),
    // Pass through these two interior waypoints, making an 's' curve path
    {frc::Translation2d(1_m, 1_m), frc::Translation2d(2_m, -1_m)},
    // End 3 meters straight ahead of where we started, facing forward
    frc::Pose2d(3_m, 0_m, frc::Rotation2d(0_deg)),
    // Pass the config
    config);
  */

  frc2::RamseteCommand ramseteCommand(
      main_trajectory, [this]() { return m_drive.GetPose(); },
      frc::RamseteController(AutoConstants::kRamseteB,
                             AutoConstants::kRamseteZeta),
      frc::SimpleMotorFeedforward<units::meters>(
          DriveConstants::ks, DriveConstants::kv, DriveConstants::ka),
      DriveConstants::kDriveKinematics,
      [this] { return m_drive.GetWheelSpeeds(); },
      frc2::PIDController(DriveConstants::kPDriveVel, 0, 0),
      frc2::PIDController(DriveConstants::kPDriveVel, 0, 0),
      [this](auto left, auto right) { m_drive.TankDriveVolts(left, right); },
      {&m_drive});

  // Reset odometry to the starting pose of the trajectory
  m_drive.ResetOdometry(main_trajectory.InitialPose());

  // No auto
  return new frc2::SequentialCommandGroup(
      std::move(ramseteCommand),
      frc2::InstantCommand([this] { m_drive.TankDriveVolts(0_V, 0_V); }, {}));
}

void RobotContainer::SelectTrajectoryPath() {
  StartsWithLetterMode which_mode = A_GALACTIC_SEARCH;

  switch (which_mode) {
    case A_GALACTIC_SEARCH:
      if (ultrasonic.GetRangeInches() > FieldMetrics::kClosestDistanceA) {
        the_endway.SetWay(the_endway.kPathAblue);
      } else {
        the_endway.SetWay(the_endway.kPathAred);
      }
      break;
    case B_GALACTIC_SEARCH:
      if (ultrasonic.GetRangeInches() > FieldMetrics::kClosestDistanceB) {
        the_endway.SetWay(the_endway.kPathBblue);
      } else {
        the_endway.SetWay(the_endway.kPathBred);
      }
      break;
    case BARREL_TASK:
      the_endway.SetWay(the_endway.kPathBarrel);
      break;
    case BOUNCE_TASK:
      the_endway.SetWay(the_endway.kPathBounce);
      break;
    case SLALOM_PATH:
      the_endway.SetWay(the_endway.kPathSlalom);
      break;
  }
}