// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "subsystems/sh00ter-subsystem.h"

using namespace DriveConstants;

OuttakeSubsystem::OuttakeSubsystem()
    : outtake_motor1{kShooterNeo1, rev::CANSparkMax::MotorType::kBrushless},
      outtake_motor2{kShooterNeo2, rev::CANSparkMax::MotorType::kBrushless} {}

void OuttakeSubsystem::Shoot() {}

void OuttakeSubsystem::Rotate() {}
