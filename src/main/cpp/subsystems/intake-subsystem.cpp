// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "subsystems/intake-subsystem.h"

#include <frc2/command/CommandScheduler.h>

using namespace DriveConstants;

IntakeSubsystem::IntakeSubsystem()
    : intake{kIntakeMotorMainPort}, conveyer{kConveyerMotorMainPort} {}

void IntakeSubsystem::RunSafe() {
  intake.Set(ControlMode::PercentOutput, safe_intake_power);
  conveyer.Set(ControlMode::PercentOutput, safe_conveyer_power);
}

void IntakeSubsystem::RunMax() {
  intake.Set(ControlMode::PercentOutput, max_intake_power);
  conveyer.Set(ControlMode::PercentOutput, max_conveyer_power);
}