// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "subsystems/drive-subsystem.h"

#include <frc/geometry/Rotation2d.h>
#include <frc/kinematics/DifferentialDriveWheelSpeeds.h>

using namespace DriveConstants;

DriveSubsystem::DriveSubsystem()
    : m_left1{kLeftMotor1Port},
      m_left2{kLeftMotor2Port},
      m_right1{kRightMotor1Port},
      m_right2{kRightMotor2Port},
      m_left_encoder{kLeftEncoderPorts[0], kLeftEncoderPorts[1]},
      m_right_encoder{kRightEncoderPorts[0], kRightEncoderPorts[1]},
      m_odometry{m_gyro->GetRotation2d()} {
  // Set the distance per pulse for the encoders
  m_left_encoder.SetDistancePerPulse(kEncoderDistancePerPulse);
  m_right_encoder.SetDistancePerPulse(kEncoderDistancePerPulse);

  ResetEncoders();
}

void DriveSubsystem::Periodic() {
  // Implementation of subsystem periodic method goes here.
  m_odometry.Update(m_gyro->GetRotation2d(),
                    units::meter_t(m_left_encoder.GetDistance()),
                    units::meter_t(m_right_encoder.GetDistance()));
}

void DriveSubsystem::ArcadeDrive(double fwd, double rot) {
  m_drive.ArcadeDrive(fwd, rot);
}

void DriveSubsystem::TankDriveVolts(units::volt_t left, units::volt_t right) {
  m_left_motors.SetVoltage(left);
  m_right_motors.SetVoltage(-right);
  m_drive.Feed();
}

void DriveSubsystem::ResetEncoders() {
  m_left_encoder.Reset();
  m_right_encoder.Reset();
}

double DriveSubsystem::GetAverageEncoderDistance() {
  return (m_left_encoder.GetDistance() + m_right_encoder.GetDistance()) / 2.0;
}

frc::Encoder& DriveSubsystem::GetLeftEncoder() { return m_left_encoder; }

frc::Encoder& DriveSubsystem::GetRightEncoder() { return m_right_encoder; }

void DriveSubsystem::SetMaxOutput(double maxOutput) {
  m_drive.SetMaxOutput(maxOutput);
}

units::degree_t DriveSubsystem::GetHeading() const {
  return m_gyro->GetRotation2d().Degrees();
}

double DriveSubsystem::GetTurnRate() { return -m_gyro->GetRate(); }

frc::Pose2d DriveSubsystem::GetPose() { return m_odometry.GetPose(); }

frc::DifferentialDriveWheelSpeeds DriveSubsystem::GetWheelSpeeds() {
  return {units::meters_per_second_t(m_left_encoder.GetRate()),
          units::meters_per_second_t(m_right_encoder.GetRate())};
}

void DriveSubsystem::ResetOdometry(frc::Pose2d pose) {
  ResetEncoders();
  m_odometry.ResetPosition(pose, m_gyro->GetRotation2d());
}