# -*- coding: UTF-8 -*-
"""Robot's vision 2021.

Detects figure and calculates distance + angle for gun to rotate
"""
import logging
import time
from typing import Any, List

import cv2
import numpy as np
from analyzer import Analyzer
from config import CameraSettings
from filters import Filter
from networktables import NetworkTables

CONF_SETUP = CameraSettings()

# Logger config
if CONF_SETUP.log_in_file:
    logging.basicConfig(
        filename="app.log",
        filemode="w",
        format=CONF_SETUP.log_stream_string_format,
    )
else:
    logging.basicConfig(format=CONF_SETUP.log_stream_string_format)

logging.getLogger().setLevel(logging.INFO)

if not CONF_SETUP.from_file:
    vs = cv2.VideoCapture(CONF_SETUP.video_source)
    time.sleep(CONF_SETUP.warmup_time)


class FinalRenderer:
    """Realizes two ways of rendering all of the stuff on the screen.

    Splitted or only in the one window
    """

    def __init__(self, f_collection: Filter, which_filters: List[Any], conf_setup: CameraSettings) -> None:
        """Filter class and list with its in usage functions."""
        self.f_collection = f_collection
        self.filters_in_use = which_filters
        self.conf_setup = conf_setup

        # Table for vision output information
        self.vision_nt = NetworkTables.getTable("Vision")

    def draw_helpful_lines(self, *on) -> None:
        """Draws vertical and horizontal centered lines."""
        for el_img in on:
            cur_height = el_img.shape[0]
            cur_width = el_img.shape[1]

            cv2.line(
                el_img,
                (cur_width - 1, 0),
                (cur_width - 1, cur_height),
                self.conf_setup.demarcation_color,
                self.conf_setup.demarcation_width,
            )
            cv2.line(
                el_img,
                (cur_width // 2, 0),
                (cur_width // 2, cur_height),
                self.conf_setup.horizontal_center_color,
                self.conf_setup.horizontal_center_width,
            )
            cv2.line(
                el_img,
                (0, cur_height // 2),
                (cur_width, cur_height // 2),
                self.conf_setup.vertical_center_color,
                self.conf_setup.vertical_center_width,
            )

    def draw_helpful_rect(self, rect: Any, line_x: int, *on) -> None:
        """Draws a rectangle - AKA founded object."""
        target_center = line_x

        for el_img in on:
            cv2.rectangle(
                el_img,
                (rect[0], rect[1]),
                (rect[0] + rect[2], rect[1] + rect[3]),
                self.conf_setup.rect_color,
                self.conf_setup.rect_width,
            )

            if target_center:
                cv2.line(
                    el_img,
                    (target_center, rect[1] - 20),
                    (target_center, rect[1] + rect[3] + 20),
                    self.conf_setup.object_center_color,
                    self.conf_setup.object_center_width,
                )

    def calculating_trigger(self, analytics) -> None:
        """Calculate meta-properties and send it if needed."""
        distance = analytics.distance
        angle = analytics.real_angle

        if self.conf_setup.calculate_metrics:
            logging.info("distance: %f", distance)
            logging.info("angle: %f", angle)
        if self.conf_setup.communicate_via_tables:
            self.vision_nt.putNumber("distance", distance)

    def render_screen_separate(self) -> Any:
        """Render filter in seperated tiled windows."""
        cur_frame = self.frame
        analyzer = Analyzer(cur_frame, self.conf_setup)

        if self.conf_setup.draw_rect:
            rect = analyzer.rectangle
            target_center = int(rect[0] + rect[2] / 2)

        if len(self.filters_in_use) % 2 != 0:
            if self.f_collection.black_filter in self.filters_in_use:
                self.filters_in_use.remove(self.f_collection.black_filter)
            else:
                self.filters_in_use.append(self.f_collection.black_filter)

        prev = None
        screen = None

        for f_name in self.filters_in_use:
            if f_name is Filter.mask_filter:
                el_img = f_name(cur_frame, self.conf_setup)
            else:
                el_img = f_name(cur_frame)

            if self.conf_setup.draw_lines:
                self.draw_helpful_lines(el_img)
            if self.conf_setup.draw_rect:
                self.draw_helpful_rect(rect, target_center, el_img)

            if prev is None:
                prev = el_img
            else:
                row = np.concatenate((prev, el_img), axis=1)
                prev = None

                if screen is None:
                    screen = row
                else:
                    screen = np.concatenate((screen, row), axis=0)

        self.calculating_trigger(analyzer)
        return screen

    def render_screen_in_one(self) -> Any:
        """Render filter in the only one window."""
        cur_frame = self.frame
        analyzer = Analyzer(cur_frame, self.conf_setup)

        if self.conf_setup.draw_rect:
            rect = analyzer.rectangle
            target_center = int(rect[0] + rect[2] / 2)

        screen = None

        for _, f_name in enumerate(self.filters_in_use):
            if f_name is Filter.mask_filter:
                el_img = f_name(cur_frame, self.conf_setup)
            else:
                el_img = f_name(cur_frame)

            if screen is None:
                screen = el_img
            else:
                k = _ / (_ + 1)
                screen = cv2.addWeighted(screen, k, el_img, 1 - k, 0)

        if self.conf_setup.draw_lines:
            self.draw_helpful_lines(screen)
        if self.conf_setup.draw_rect:
            self.draw_helpful_rect(rect, target_center, screen)

        self.calculating_trigger(analyzer)
        return screen

    def mouse_callback(self, event: int, x_coor: int, y_coor: int, flags: Any, param: Any) -> None:
        """Mouse callback for OpenCV window. Gets calibrated colors."""
        if self.conf_setup.calibration and event == cv2.EVENT_LBUTTONDOWN:
            if self.conf_setup.calibrate_first_c is None:
                self.conf_setup.calibrate_first_c = self.frame[y_coor][x_coor]
            else:
                self.conf_setup.calibrate_second_c = self.frame[y_coor][x_coor]
                if self.conf_setup.convert_color_model_name:
                    first_color = cv2.cvtColor(
                        np.uint8([[self.conf_setup.calibrate_first_c]]),
                        self.conf_setup.convert_color,
                    )[0][0]
                    second_color = cv2.cvtColor(
                        np.uint8([[self.conf_setup.calibrate_second_c]]),
                        self.conf_setup.convert_color,
                    )[0][0]
                else:
                    first_color = self.conf_setup.calibrate_first_c
                    second_color = self.conf_setup.calibrate_second_c

                lower_bound = np.array(
                    tuple(
                        map(min, zip(first_color, second_color)),
                    ),
                )
                upper_bound = np.array(
                    tuple(
                        map(max, zip(first_color, second_color)),
                    ),
                )

                logging.info("lower: %s", (lower_bound,))
                logging.info("upper: %s", (upper_bound,))

                self.conf_setup.color_lower = lower_bound
                self.conf_setup.color_upper = upper_bound

                self.conf_setup.calibrate_first_c = None
                self.conf_setup.calibrate_second_c = None
                self.conf_setup.pause_on_off = False
                self.conf_setup.calibration = False

    def handle_key(self, key: int) -> None:
        """Do an action which depends on key pressed."""
        # Default buttons
        match key:
            case ord("n"):
                self.conf_setup.color_lower = tuple(
                  map(
                      lambda a: int(a),
                      input().split(" "),
                  ),
                )
                self.conf_setup.color_upper = tuple(
                  map(
                      lambda a: int(a),
                      input().split(" "),
                  ),
                )

                logging.info("lower: %s", self.conf_setup.color_lower)
                logging.info("upper: %s", self.conf_setup.color_upper)
            case ord("i"):
                cv2.imwrite(
                    self.conf_setup.file_on_save_name_path,
                    self.current_screen,
                )
            case ord("k"):
                self.conf_setup.calibration = True
            case ord("l"):
                self.conf_setup.draw_lines = not self.conf_setup.draw_lines
            case ord("b"):
                self.conf_setup.draw_rect = not self.conf_setup.draw_rect
            case ord("d"):
                self.conf_setup.calculate_metrics = (
                   not self.conf_setup.calculate_metrics
                )
            case ord("f"):
                # Filters match
                if self.f_collection.no_filter in self.filters_in_use:
                    self.filters_in_use.remove(self.f_collection.no_filter)
                else:
                    self.filters_in_use.append(self.f_collection.no_filter)
            case ord("o"):
                # Original image
                if self.f_collection.original_filter in self.filters_in_use:
                    self.filters_in_use.remove(
                      self.f_collection.original_filter
                      )
                else:
                    self.filters_in_use.append(
                      self.f_collection.original_filter
                      )
            case ord("m"):
                if self.f_collection.mask_filter in self.filters_in_use:
                    self.filters_in_use.remove(self.f_collection.mask_filter)
                else:
                    self.filters_in_use.append(self.f_collection.mask_filter)
            case ord("h"):
                if self.f_collection.hue_filter in self.filters_in_use:
                    self.filters_in_use.remove(self.f_collection.hue_filter)
                else:
                    self.filters_in_use.append(self.f_collection.hue_filter)
            case ord("s"):
                if self.f_collection.saturation_filter in self.filters_in_use:
                    self.filters_in_use.remove(
                      self.f_collection.saturation_filter
                      )
                else:
                    self.filters_in_use.append(
                      self.f_collection.saturation_filter
                      )
            case ord("v"):
                if self.f_collection.value_filter in self.filters_in_use:
                    self.filters_in_use.remove(self.f_collection.value_filter)
                else:
                    self.filters_in_use.append(self.f_collection.value_filter)
            case ord("r"):
                if self.f_collection.rainbow_filter in self.filters_in_use:
                    self.filters_in_use.remove(
                      self.f_collection.rainbow_filter
                      )
                else:
                    self.filters_in_use.append(
                      self.f_collection.rainbow_filter
                      )
            case ord("g"):
                self.conf_setup.ball_trigger = not self.conf_setup.ball_trigger
            case ord(" "):
                # Space
                self.conf_setup.pause_on_off = not self.conf_setup.pause_on_off

    @property
    def configuration(self) -> Any:
        """Basically a configuration setup."""
        return self.conf_setup

    @property
    def frame(self) -> Any:
        """Get correct frame which depends on resolution, source etc."""
        if self.conf_setup.from_file:
            cur_frame = cv2.imread(self.conf_setup.from_file)
        else:
            check_if_ok, cur_frame = vs.read()
        if self.conf_setup.res_width or self.conf_setup.res_width:
            if not self.conf_setup.res_width:
                self.conf_setup.res_width = cur_frame.shape[1]
            if not self.conf_setup.res_width:
                self.conf_setup.res_width = cur_frame.shape[0]

            cur_frame = cv2.resize(
                cur_frame,
                (self.conf_setup.res_width, self.conf_setup.res_width),
            )
        elif self.conf_setup.res_fx != 1 or self.conf_setup.res_fy != 1:
            cur_frame = cv2.resize(
                cur_frame,
                (0, 0),
                fx=self.conf_setup.res_fx,
                fy=self.conf_setup.res_fy,
            )

        cur_frame = cv2.GaussianBlur(
            cur_frame,
            (self.conf_setup.gaussian_blur_x, self.conf_setup.gaussian_blur_y),
            0,
        )

        return cur_frame

    @property
    def image(self) -> Any:
        """Get an image with converted / default color model on it."""
        if self.configuration.convert_color_model_name is not None:
            # Could be wrong converted color model?
            img = cv2.cvtColor(
                self.frame,
                self.configuration.convert_color_model_name,
            )
        else:
            img = self.frame

        return img

    @property
    def current_screen(self) -> Any:
        """Get current screen separated or just in one (JIO???)."""
        if self.configuration.filter_mode:
            cur_screen = self.render_screen_in_one()
        else:
            cur_screen = self.render_screen_separate()

        return cur_screen


class Recording:
    """Starts an unstoppable while loop and mixes all the data.

    Like structs and data/classes above
    """

    def __init__(self, f_collection: Filter, filters_in_use: List[Filter], conf_setup: CameraSettings) -> None:
        """Photo filters."""
        self.f_collection = f_collection
        self.filters_in_use = filters_in_use
        self.conf_setup = conf_setup

    @staticmethod
    def time_render_process(fps, speed, diff) -> int:
        """Calculate time to esteem if we don't go full speed recording."""
        return 1 / (fps * speed) - diff

    def start_video_loop(self) -> None:
        """Start a main video loop which handles cv2 stuff."""
        # Creating a necessary class instance of renderer
        render = FinalRenderer(
            self.f_collection,
            self.filters_in_use,
            self.conf_setup,
        )

        # Set window
        cv2.namedWindow(self.conf_setup.window_title)
        cv2.setMouseCallback(
            self.conf_setup.window_title,
            render.mouse_callback,
        )

        # Main loop
        while True:
            f_start = time.time()
            which_key = cv2.waitKey(1) & 0xFF

            if not render.configuration.pause_on_off:
                cur_screen = render.current_screen

                if cur_screen is not None:
                    cv2.imshow(render.configuration.window_title, cur_screen)
                if (
                    render.configuration.from_file
                    and render.configuration.from_file_autopause
                ):
                    render.configuration.pause_on_off = True

            if which_key != ord("q"):
                render.handle_key(which_key)
            else:
                break

            if render.configuration.is_debug:
                f_end = time.time()
                f_diff = f_end - f_start
                f_timeleft = Recording.time_render_process(
                    render.configuration.fps,
                    render.configuration.speed,
                    f_diff,
                )

                if f_timeleft > 0:
                    time.sleep(f_timeleft)

        cv2.destroyAllWindows()


# Entry point for the whole program
if __name__ == "__main__":
    filters = Filter()
    needed_filters = [filters.no_filter, filters.original_filter]

    recorder = Recording(filters, needed_filters, CONF_SETUP)
    recorder.start_video_loop()
