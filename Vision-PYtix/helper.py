# -*- coding: UTF-8 -*-
"""It helps but just for a little with calibrating.

(BTW NO TKinter realization!).
"""
import time

import cv2
import numpy as np
from config import CameraSettings


class GUIdoOne:
    """Just a simple GUI."""

    def __init__(
        self,
        warmup_time: int,
        name_input: str,
        name_output: str,
        camera_serial_port: int,
    ):
        """Get name of the windows and select camera."""
        self.name_input = name_input
        self.name_output = name_output
        self.thanks_cap = cv2.VideoCapture(camera_serial_port)
        time.sleep(warmup_time)

    @staticmethod
    def do_nothing(*arg):
        """Temporary stag for cv2 trackbar."""
        pass

    @property
    def return_cap(self):
        """Return an already read cap."""
        return self.thanks_cap.read()

    def create_layout(self):
        """Create a basic menu layout."""
        cv2.namedWindow(self.name_output)
        cv2.namedWindow(self.name_input)

        cv2.createTrackbar("h1", self.name_input, 0, 255, GUIdoOne.do_nothing)
        cv2.createTrackbar("s1", self.name_input, 0, 255, GUIdoOne.do_nothing)
        cv2.createTrackbar("v1", self.name_input, 0, 255, GUIdoOne.do_nothing)
        cv2.createTrackbar(
            "h2",
            self.name_input,
            255,
            255,
            GUIdoOne.do_nothing,
        )
        cv2.createTrackbar(
            "s2",
            self.name_input,
            255,
            255,
            GUIdoOne.do_nothing,
        )
        cv2.createTrackbar(
            "v2",
            self.name_input,
            255,
            255,
            GUIdoOne.do_nothing,
        )

    def start_tracking(self):
        """Start a tracker loop."""
        while True:
            img = self.return_cap[1]
            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

            h1 = cv2.getTrackbarPos("h1", self.name_input)
            s1 = cv2.getTrackbarPos("s1", self.name_input)
            v1 = cv2.getTrackbarPos("v1", self.name_input)
            h2 = cv2.getTrackbarPos("h2", self.name_input)
            s2 = cv2.getTrackbarPos("s2", self.name_input)
            v2 = cv2.getTrackbarPos("v2", self.name_input)

            h_min = np.array((h1, s1, v1), np.uint8)
            h_max = np.array((h2, s2, v2), np.uint8)

            trash = cv2.inRange(hsv, h_min, h_max)
            cv2.imshow(self.name_output, trash)

            ch = cv2.waitKey(5)
            if ch == 27 or ch == ord("q"):
                break

        self.destroy_and_close()

    def destroy_and_close(self):
        """Release camera and destroy (aka close proccesses of) all windows."""
        self.thanks_cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    program = GUIdoOne(CameraSettings.warmup_time, "Inf...", "Answ?", 0)
    program.create_layout()
    program.start_tracking()
