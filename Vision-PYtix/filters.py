# -*- coding: UTF-8 -*-
"""Collection of useful functions.

They are applied to image to calibrate the final color
*Really useful thing one...
"""
import cv2
import numpy as np
from analyzer import Analyzer
from config import CameraSettings
from numba import jit


class Filter:
    """Class with all of these filters.

    Every method SHOULD be static.
    And as every method is static, no need for __init__ at all
    """

    @staticmethod
    def original_filter(img: np.ndarray) -> np.ndarray:
        """Return original img."""
        return img

    @staticmethod
    def no_filter(img: np.ndarray) -> np.ndarray:
        """No-filter filter."""
        if CameraSettings.convert_color_reverse:
            img = cv2.cvtColor(img, CameraSettings.convert_color_reverse)

        return img

    @staticmethod
    def mask_filter(img: np.ndarray, conf) -> np.ndarray:
        """Mask filter."""
        mask = Analyzer.find_mask(img, conf)
        frame_mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

        return frame_mask

    @staticmethod
    def hue_filter(img: np.ndarray) -> np.ndarray:
        """H parameter filter."""
        h_param = cv2.split(img)[0]
        image_h = cv2.cvtColor(h_param, cv2.COLOR_GRAY2BGR)

        return image_h

    @staticmethod
    def saturation_filter(img: np.ndarray) -> np.ndarray:
        """S parameter filter."""
        s_param = cv2.split(img)[1]
        image_s = cv2.cvtColor(s_param, cv2.COLOR_GRAY2BGR)

        return image_s

    @staticmethod
    def value_filter(img: np.ndarray) -> np.ndarray:
        """V parameter filter."""
        v_param = cv2.split(img)[2]
        image_v = cv2.cvtColor(v_param, cv2.COLOR_GRAY2BGR)

        return image_v

    @staticmethod
    def rainbow_filter(img: np.ndarray) -> np.ndarray:
        """Rainbow filter."""
        h_param, s_param, v_param = cv2.split(img)

        s_param.fill(255)
        v_param.fill(255)

        image_hsv = cv2.merge([h_param, s_param, v_param])
        image_r = cv2.cvtColor(image_hsv, cv2.COLOR_HSV2BGR)

        return image_r

    @staticmethod
    @jit(nopython=True)
    def black_filter(img: np.ndarray) -> np.ndarray:
        """Black (all zeros) filter."""
        return np.zeros(img.shape, np.uint8)
