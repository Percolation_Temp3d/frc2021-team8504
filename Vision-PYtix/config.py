# -*- coding: UTF-8 -*-
"""Just a config file for vision.py.

Some color's settings defined, logger format (maybe), cv2 options etc...
"""
from dataclasses import dataclass
from typing import Any

import cv2


@dataclass
class CameraSettings:
    """Collection of constants and non-constants (genius move).

    Visualize it as C-like struct;
    I mostly represent python dataclasses in that way
    """

    # Debug mode
    is_debug: bool = False
    ball_trigger: bool = False
    communicate_via_tables: bool = True

    # Filter mode
    filter_mode: int = 0
    video_source: int = 0
    warmup_time: int = 3

    # Log source, format
    log_in_file: bool = False
    log_stream_string_format: str = (
        "%(asctime)s - %(name)s - %(levelname)s: %(message)s"
    )

    # Videofile settings
    from_file: Any = None
    from_file_autopause: bool = False
    file_on_save_name_path = "test.png"

    # Speed, works only in debug mode
    fps: int = 5
    speed: int = 4

    # Pause
    pause_on_off: int = False

    # Resolution
    res_width: Any = None
    res_height: Any = None
    res_fx: int = 1
    res_fy: int = 1

    # Color model
    convert_color_model_name: str = "HSV"

    # Colors of object
    color_lower: tuple = (20, 100, 100)
    color_upper: tuple = (30, 255, 255)

    # Helpful lines
    draw_lines: bool = True
    draw_rect: bool = True

    # Should be calculated variables
    calculate_metrics: bool = True

    # Color
    rect_color: tuple = (0, 255, 255)
    rect_width: int = 2
    horizontal_center_color: tuple = (255, 255, 255)
    horizontal_center_width: int = 1
    vertical_center_color: tuple = (255, 255, 255)
    vertical_center_width: int = 1
    demarcation_color: tuple = (255, 0, 255)
    demarcation_width: int = 1
    object_center_color: tuple = (255, 0, 255)
    object_center_width: int = 2

    # Erase resize metrics
    erode_iterations: int = 3
    dilate_iterations: int = 3
    opening_iterations: int = 1
    closing_iterations: int = 1

    # Blur metrics, should be odd
    gaussian_blur_x: int = 5
    gaussian_blur_y: int = 5

    # Program's title
    window_title: str = "Vision"

    # Get objects by name
    convert_color_str: str = f"COLOR_BGR2{convert_color_model_name}"
    convert_color: int = getattr(cv2, convert_color_str)
    convert_color_reverse_str: str = f"COLOR_{convert_color_model_name}2BGR"
    convert_color_reverse: int = getattr(cv2, convert_color_reverse_str)

    # Color calibration
    calibration: bool = False
    calibrate_first_c: Any = None
    calibrate_second_c: Any = None

    # Object size
    actual_object_size: float = 0.1
    coefficient: int = 115
    is_object_found: bool = False
