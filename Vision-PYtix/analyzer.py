# -*- coding: UTF-8 -*-
"""Yes, I did create that file only because it prevents cycle import.

That was caused because of a filters.py and vision.py files
"""
import logging
from math import atan
from typing import Any

import cv2
import numpy as np


class Analyzer:
    """Analyzes an images, applies the mask, finds a rectangle.

    Also calculates a distance and an angle for a gun to rotate
    """

    def __init__(self, captured_pict: np.ndarray, configuration: Any):
        """Init CameraSettings() and captured from FinalRenderer photo."""
        self.img = captured_pict
        self.conf_setup = configuration

    @staticmethod
    def find_mask(img: np.ndarray, conf: np.ndarray) -> Any:
        """Apply mask for an unmasked image."""
        mask = cv2.inRange(img, conf.color_lower, conf.color_upper)

        # Morphological matrix for providing more understandable operations
        kernel = np.ones((3, 3), np.uint8)

        # Delete noise, better use kernel then None as a second argument
        mask = cv2.erode(mask, kernel, iterations=conf.erode_iterations)
        mask = cv2.dilate(mask, kernel, iterations=conf.dilate_iterations)
        mask = cv2.morphologyEx(
            mask,
            cv2.MORPH_OPEN,
            kernel,
            iterations=conf.opening_iterations,
        )
        mask = cv2.morphologyEx(
            mask,
            cv2.MORPH_CLOSE,
            kernel,
            iterations=conf.closing_iterations,
        )

        return mask

    @property
    def rectangle(self) -> Any:
        """Get our final rectangle after we apply mask."""
        non_zero_pixels = cv2.findNonZero(
            Analyzer.find_mask(self.img, self.conf_setup),
        )
        rect = cv2.boundingRect(non_zero_pixels)

        return rect

    @property
    def distance(self) -> float:
        """Calculate the distance between the cam and a basket.

        TODO: get it done right, also re elaborate coefficient
        """
        try:
            distance = (
                self.img.shape[0]
                * self.conf_setup.actual_object_size
                * self.conf_setup.coefficient
                / self.rectangle[3]
            )
            self.conf_setup.is_object_found = True
        except ZeroDivisionError:
            distance = -1
            self.conf_setup.is_object_found = False

            if self.conf_setup.ball_trigger:
                logging.warning("nothing is found on picture")

        return distance

    @property
    def possible_angle(self) -> float:
        """Calculate the angle between the camera and an object.

        TODO: get the needed formula from excel by calculating a hype line?
        """
        try:
            ans_angle = atan(self.rectangle[3] / self.distance)
        except ZeroDivisionError:
            ans_angle = 0
            logging.warning("Object is too close")

        return ans_angle

    @property
    def real_angle(self) -> float:
        """Calculate the real angle beetween the gun and an object.

        As robot's gun is placed much lower then the camera, the angle
        to rotate the gun should be just a little bit bigger.
        Also should we not forget about F=mg + friction force?
        """
        try:
            result = self.possible_angle + 10
        except ZeroDivisionError:
            result = 0

        return result
